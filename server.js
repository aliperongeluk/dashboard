const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 1234;

app.use(express.static(path.join(__dirname, 'dist')));
app.use((req, res) => res.sendFile(`${__dirname}/dist/index.html`));

//start server
app.listen(port, (req, res) => {
  console.log(`server listening on port: ${port}`);
});
