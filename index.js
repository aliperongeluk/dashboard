import React from 'react';
import ReactDom from 'react-dom';
import App from './src/App';

ReactDom.render(<App />, document.getElementById('app'));

// Hot Module Replacement
// eslint-disable-next-line no-undef
if (module.hot) {
    // eslint-disable-next-line no-undef
    module.hot.accept();
}
