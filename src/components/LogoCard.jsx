import React from 'react';

import Logo from './Logo';

const LogoCard = () => {
  return (
    <div className="bg-white rounded-lg shadow-lg max-w-5xl mx-auto text-center py-5">
      <Logo />
    </div>
  );
};

export default LogoCard;
