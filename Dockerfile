FROM node:10.13.0

WORKDIR /home/docktor/compositions/dashboard

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

EXPOSE 1234

CMD ["node", "server.js"]
