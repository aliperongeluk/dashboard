import axios from 'axios';
import knownServices from '../constants/knownServices';

export const getServices = async () => {
  const eurekaData = await axios.get(
    'http://eureka.aliperongeluk.eu/eureka/apps',
    {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers':
          'Origin, X-Requested-With, Content-Type, Accept',
        'Cache-Control': 'private, no-store, max-age=0',
      },
    }
  );
  const applications = eurekaData.data.applications.application;
  const services = [];

  try {
    pushInstancesInServices(applications, services);
    pushOfflineServicesInServices(services);
  } catch (e) {
    //  Not a known service
  }
  services.sort(sortServicesByTitle);

  return services;
};

const pushInstancesInServices = (applications, services) => {
  applications.forEach(application => {
    application.instance.forEach(instance => {
      const known =
        knownServices[
          knownServices.findIndex(known => known.key === instance.app)
        ];
      services.push({
        ...instance,
        ...known,
        online: instance.status === 'UP',
      });
    });
  });
};

const pushOfflineServicesInServices = services => {
  knownServices.forEach(known => {
    if (services.findIndex(service => service.app === known.key) === -1) {
      services.push({
        ...known,
        online: false,
      });
    }
  });
};

const sortServicesByTitle = (a, b) => {
  try {
    const x = a.title.toLowerCase();
    const y = b.title.toLowerCase();
    if (x < y) {
      return -1;
    }
    if (x > y) {
      return 1;
    }
    return 0;
  } catch (error) {
    return 0;
  }
};

export const getServiceById = async (id, instance) => {
  const services = await getServices();
  if (id && instance) {
    return services[
      services.findIndex(s => s.key === id && s.instanceId === instance)
    ];
  } else {
    return services[services.findIndex(s => s.key === id)];
  }
};
