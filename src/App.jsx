import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './containers/Home';
import NotFound from './containers/NotFound';
import Detail from './containers/Detail';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faSatelliteDish,
  faSkull,
  faShoppingBag,
  faCode,
  faSyncAlt,
} from '@fortawesome/free-solid-svg-icons';

import { faGitlab, faJenkins } from '@fortawesome/free-brands-svg-icons';

library.add(
  faSatelliteDish,
  faSkull,
  faShoppingBag,
  faGitlab,
  faCode,
  faJenkins,
  faSyncAlt
);

const App = () => {
  return (
    <div className="min-w-screen min-h-screen bg-gray-100">
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/detail/:id/:instance?" component={Detail} />
          <Route path="*" exact component={NotFound} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
