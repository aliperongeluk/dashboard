import React from 'react';

const MicroserviceListLoading = () => {
  return (
    <div className="p-8">
      {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(i => (
        <div
          key={i}
          className="flex border-b border-gray-300 p-4 bg-blue-100 h-12"
        />
      ))}
    </div>
  );
};

export default MicroserviceListLoading;
