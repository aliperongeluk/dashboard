import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Logo = () => {
  return (
    <a
      href="/"
      className="bg-blue-400 w-auto inline-block cp-parallelogram text-white text-center p-4 sm:p-6">
      <h1 className="text-xl sm:text-2xl mx-20">
        <FontAwesomeIcon icon="shopping-bag" className="mr-2" /> AliPerOngeluk
      </h1>
    </a>
  );
};

export default Logo;
