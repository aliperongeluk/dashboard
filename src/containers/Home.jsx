import React from 'react';
import PropTypes from 'prop-types';
import useAsync from 'react-use/lib/useAsync';

import MicroserviceList from '../components/MicroserviceList';
import MicroserviceListLoading from '../components/MicroserviceListLoading';
import LogoCard from '../components/LogoCard';

import { getServices } from '../microservices/request';

const Home = ({ history }) => {
  const services = useAsync(getServices);

  const selectService = service => {
    let path = '/detail/' + service.key;
    if (service.instanceId) {
      path = path + '/' + service.instanceId;
    }
    history.push(path);
  };

  return (
    <>
      <div className="px-3 py-5">
        <LogoCard />
        <div className="bg-white rounded-lg shadow-lg max-w-5xl mx-auto mt-5">
          {services.loading && <MicroserviceListLoading />}
          {services.value && (
            <MicroserviceList
              selectService={selectService}
              microservices={services.value}
            />
          )}
          {services.error && <h1 className="p-6">Error!</h1>}
        </div>
      </div>
    </>
  );
};

Home.propTypes = {
  history: PropTypes.object,
};

export default Home;
