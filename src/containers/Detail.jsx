import React from 'react';
import { useAsync } from 'react-use';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import LogoCard from '../components/LogoCard';

import { getServiceById } from '../microservices/request';

const Detail = props => {
  const getService = async () => {
    return await getServiceById(
      props.match.params.id,
      props.match.params.instance
    );
  };
  const detail = useAsync(getService);

  const renderLoading = () => {
    return (
      <div className="p-5">
        <div className="bg-blue-100 h-64" />
      </div>
    );
  };

  const renderDetail = value => {
    const {
      title,
      ipAddr,
      port,
      app,
      customUrl,
      gitlab,
      swagger,
      jenkins,
    } = value;

    return (
      <div className="p-5">
        <h1 className="text-xl sm:text-2xl font-bold mb-2">{title}</h1>
        {ipAddr && <div>IP: {ipAddr}</div>}
        {port && <div>Port: {port.$}</div>}
        {customUrl && <div>Custom URL: {customUrl}</div>}
        {app && <div>App: {app}</div>}
        <div className="block sm:flex mt-5">
          {gitlab && (
            <a
              className="flex-auto pb-2 sm:pb-0 sm:pr-3"
              rel="noopener noreferrer"
              target="_blank"
              href={gitlab}>
              <button className="bg-orange-500 hover:bg-orange-600 t-background flex-auto w-full text-white p-3 shadow">
                <FontAwesomeIcon
                  className="mr-2 text-lg"
                  icon={['fab', 'gitlab']}
                />{' '}
                Gitlab
              </button>
            </a>
          )}

          {jenkins && (
            <a
              className="flex-auto pt-2 sm:pt-0 "
              rel="noopener noreferrer"
              target="_blank"
              href={jenkins}>
              <button className="bg-blue-400 hover:bg-blue-500 t-background flex-auto w-full text-white p-3 shadow">
                <FontAwesomeIcon
                  className="mr-2 text-lg"
                  icon={['fab', 'jenkins']}
                />{' '}
                Jenkins
              </button>
            </a>
          )}

          {swagger && (
            <a
              className="flex-auto pt-2 sm:pt-0 sm:pl-3"
              rel="noopener noreferrer"
              target="_blank"
              href={swagger}>
              <button className="bg-green-500 hover:bg-green-600 t-background flex-auto w-full text-white p-3 shadow">
                <FontAwesomeIcon className="mr-2 text-lg" icon="code" /> Swagger
              </button>
            </a>
          )}
        </div>
      </div>
    );
  };

  const renderError = () => {
    return (
      <div className="p-5">
        <p>Oops something went wrong</p>
      </div>
    );
  };

  return (
    <>
      <div className="px-3 py-5">
        <LogoCard />

        <div className="bg-white rounded-lg shadow-lg max-w-5xl mx-auto mt-5">
          {detail.loading && renderLoading()}
          {detail.value && renderDetail(detail.value)}
          {detail.error && renderError()}
        </div>
      </div>
    </>
  );
};

Detail.propTypes = {
  match: PropTypes.object,
};

export default Detail;
