import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const MicroserviceListItem = ({ item, selectService }) => {
  const { title, online } = item;

  const onItemClick = () => {
    selectService(item);
  };

  return (
    <div
      className="flex border-b border-gray-300 p-4 hover:bg-blue-100 t-background"
      onClick={onItemClick}>
      <div className="flex-auto ">
        <p className="">{title}</p>
      </div>
      <div className="flex-auto text-right">
        {online && (
          <FontAwesomeIcon className="text-green-500" icon="satellite-dish" />
        )}
        {!online && <FontAwesomeIcon className="text-red-500" icon="skull" />}
      </div>
    </div>
  );
};

MicroserviceListItem.propTypes = {
  item: PropTypes.object.isRequired,
  selectService: PropTypes.func.isRequired,
};

export default MicroserviceListItem;
