import React from 'react';

import LogoCard from '../components/LogoCard';

const NotFound = () => {
  return (
    <>
      <div className="px-3 py-5">
        <LogoCard />

        <div className="bg-white rounded-lg shadow-lg max-w-5xl mx-auto mt-5 text-center">
          <h1 className="text-lg font-bold p-4">This page cannot be found</h1>
        </div>
      </div>
    </>
  );
};

export default NotFound;
