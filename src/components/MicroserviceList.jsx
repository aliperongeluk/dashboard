import React from 'react';
import PropTypes from 'prop-types';

import MicroserviceListItem from './MicroserviceListItem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const MicroserviceList = ({ microservices, selectService }) => {
  return (
    <div className="p-8">
      <div className="p-4">
        <a href="/" className="text-xl sm:text-2xl font-bold select-none">
          <FontAwesomeIcon icon="sync-alt" /> Services
        </a>
      </div>
      {microservices.map((microservice, index) => (
        <MicroserviceListItem
          selectService={selectService}
          key={index}
          item={microservice}
        />
      ))}
    </div>
  );
};

MicroserviceList.propTypes = {
  microservices: PropTypes.array.isRequired,
  selectService: PropTypes.func.isRequired,
};

export default MicroserviceList;
